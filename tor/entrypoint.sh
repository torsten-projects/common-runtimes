#!/bin/sh

echo """
SOCKSPort 0.0.0.0:${SOCKS_PORT}
Log info stdout
ControlPort 0.0.0.0:${CONTROL_PORT}
HashedControlPassword $(tor --hash-password ${CONTROL_PASSWORD} | tail -n 1)
""" > /etc/tor/torrc &&
cat /etc/tor/torrc &&
tor
